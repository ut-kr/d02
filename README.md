# Wireless Message Communication
Communications supporting information exchange between people or devices have become the fastest growing segments of the communications industry. In telecommunications, a message signal is typically encoded by a digital bit stream, which is then physically transmitted to remote sites by using analogue devices with optics or audio channel, and etc. At the receiver site, the received physical signals will be decoded into the original message signals.

This project is a wireless message communication system that enables a text message to be conveyed between two software programs by making use of a visible light channel. The text message is converted into ASCII characters, with each character composed of eight binary bits, ultimately forms a stream of bits transmitted from the transmitter to the receiver. 

In addition, the receiver also recognises a set of pre-defined text messages (commands) and control an LED bar properly.
